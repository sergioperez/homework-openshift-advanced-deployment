# Engagement Journal

This document covers the details of the OpenShift deployment delivered as required by the OCP Adv Deployment course.

Each part of it describes shows that the corresponding requirement works.

## Basic POC Requirements

### Public Git repository

The git repository which contains all the work performed to deploy it is at this url:

https://gitlab.com/sergioperez/homework-openshift-advanced-deployment

### Ansible inventory

The inventory file is `deploy.yaml`, but it is required to launch `build_files.yaml` before running it.

For more details, please read the README.md document

## Basic and HA Requirements

### Ability to authenticate at the master console

It is possible to authenticate at the master console with the following users:

system:admin
sergio
amy
andrew
brian
betty

### Registry has storage attached and working

As required, registry has an attached PersistentVolume

```bash
registry-volume                            40Gi       RWX            Retain           Bound     default/registry-claim                                                2h                                          
```

### Router is configured on each infranode

A node selector "region=infra" is set for the router pods, as the selected label is set at each one of the infranodes.

```bash
[root@bastion playbooks]# oc get pods -o wide | grep router
router-1-gq457             1/1       Running   0          3h        192.199.0.79   infranode1.80be.internal
router-1-pjst5             1/1       Running   0          3h        192.199.0.68   infranode2.80be.internal
```

### PVs of different types are available for users to consume

GlusterFS is running at the support nodes in Container Native Storage mode, so it will be possible to request PV's to it.

### Ability to deploy a simple app

Just by running `oc new-app nodejs-mongo-persistent` as `sergio`, it will be deployed successfully.

```bash
[root@bastion playbooks]# oc get pods -o wide
NAME                              READY     STATUS      RESTARTS   AGE       IP            NODE
mongodb-1-qfkbl                   1/1       Running     0          5m        10.128.4.8    node2.80be.internal
nodejs-mongo-persistent-1-build   0/1       Completed   0          5m        10.131.2.8    node1.80be.internal
nodejs-mongo-persistent-1-jlmkj   1/1       Running     0          4m        10.130.4.11   node3.80be.internal
```

### There are three masters working

There are three masters at the inventory, all of them under the [masters] section.

```bash
[root@bastion playbooks]# oc get pods -o wide
NAME                          READY     STATUS    RESTARTS   AGE       IP            NODE
webconsole-84dd4bcb4d-9mmdm   1/1       Running   2          3h        10.128.0.12   master2.80be.internal
webconsole-84dd4bcb4d-jzcx6   1/1       Running   2          3h        10.129.0.10   master3.80be.internal
webconsole-84dd4bcb4d-rct2t   1/1       Running   2          3h        10.131.0.8    master1.80be.internal
```

### There are three etc instances working

etcd is running in master1, master2 and master3 under the etcd user.

### There is a load balancer to access the masters called loadbalancer.$GUID.$DOMAIN

The load balancer will be available at:

```
loadbalancer1.$GUID.internal
loadbalancer1.$GUID.example.opentlc.com
```

### There is a load balancer/DNS for both infranodes called *.apps.$GUID.$DOMAIN
??

### There are at least two infranodes, labeled env=infra

There are two infranodes, not labeled env=infra, but labeled region=infra

```bash
oc get node infranode1.80be.internal -o yaml | grep infra
    kubernetes.io/hostname: infranode1.80be.internal
    region: infra

oc get node infranode2.80be.internal -o yaml | grep infra
    kubernetes.io/hostname: infranode2.80be.internal
    region: infra
```

## Environment Configuration

### NetworkPolicy is configured and working with projects isolated by default

The NetworkPolicy plugin is the OpenShift SDN plugin at the deployed cluster. It provides multitenancy by default.

```
networkPluginName: redhat/openshift-ovs-networkpolicy
```

### Aggregated logging is configured and working

The EFK stack is installed and working. Since the cluster just uses CNS, it has been required to re-deploy the logging install playbook.

```bash
[root@bastion src]# oc get pods -n logging
NAME                                      READY     STATUS    RESTARTS   AGE
logging-curator-1-kff62                   1/1       Running   0          4m
logging-es-data-master-wtgp1dcr-1-wp46v   2/2       Running   0          3m
logging-fluentd-6b7bg                     1/1       Running   0          3m
logging-fluentd-74tx5                     1/1       Running   0          4m
logging-fluentd-ckfdp                     1/1       Running   0          3m
logging-fluentd-cqq9w                     1/1       Running   0          4m
logging-fluentd-fgrrx                     1/1       Running   0          3m
logging-fluentd-hchk8                     1/1       Running   0          3m
logging-fluentd-kl487                     1/1       Running   0          3m
logging-fluentd-pswff                     1/1       Running   0          3m
logging-fluentd-r6wp2                     1/1       Running   0          3m
logging-fluentd-vs2j9                     1/1       Running   0          3m
logging-fluentd-zbfdg                     1/1       Running   0          3m
logging-kibana-1-zcg4b                    2/2       Running   0          4m
```

### Metrics collection is configured and working

The metric collection system is installed and working. Since the cluster just uses CNS, it has been required to re-deploy the metrics install playbook.

```bash
oc get pods -n openshift-infra | grep hawkular
hawkular-cassandra-1-rqnf4   1/1       Running   0          24m
hawkular-metrics-j9lvl       1/1       Running   0          24m
```

### Router and Registry Pods run on Infranodes

All of them are running on infranodes, using the selector `region=infra`

```
$ oc get pods -o wide -n default
NAME                       READY     STATUS    RESTARTS   AGE       IP             NODE
docker-registry-1-cbtk7    1/1       Running   0          4h        10.130.0.3     infranode1.80be.internal
docker-registry-1-jf2xd    1/1       Running   0          4h        10.128.2.3     infranode2.80be.internal
router-1-gq457             1/1       Running   0          4h        192.199.0.79   infranode1.80be.internal
router-1-pjst5             1/1       Running   0          4h        192.199.0.68   infranode2.80be.internal
```

### Metrics and Logging components run on Infranodes



### Service Catalog, Template Service Broker, and Ansible Broker are all Working


## CICD Workflow

### Jenkins pod is running with a persistent volume

Jenkins is running in a pod using the image jenkins-persistent

```
$ oc get pods -n jenkins | grep jenkins
jenkins-1-m87qr   1/1       Running     0          4h
```

Using `oc get pv` and `oc get pvc` it is possible to see the PersistentVolume and the PersistentVolumeClaim of jenkins-persistent. 

```
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS    CLAIM                                   STORAGECLASS        REASON    AGE
pvc-3658dd20-85c1-11e8-972f-02aa50acef9a   1Gi        RWO            Delete           Bound     jenkins/jenkins                         glusterfs-storage             4h
```

```
$ oc get pvc
NAME      STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS        AGE
jenkins   Bound     pvc-3658dd20-85c1-11e8-972f-02aa50acef9a   1Gi        RWO            glusterfs-storage   4h
```

### Jenkins deploys `openshift-tasks app`

The deployment of the openshift-tasks app is automated using a BuildConfig with the jenkinsPipeline strategy.

The buildConfig object definition is located at the project, at ./src/playbooks/buildconfigs

Once the pipeline is ready, there are two ways of starting it, starting the pipeline from a web UI (it does not matter if it is performed from the Jenkins one, or the OpenShift one), or running:

```
oc start-build build-openshift-tasks -n jenkins
```

Of course, it is a step which should not require any manual intervention, since it is included at a playbook which is called when the platform is deployed.


### Jenkins OpenShift Plugin is used to create a CICD workflow

The BuildConfig mentioned previously has a pipeline which builds the app, runs unit tests, and deploys the app.

In a "more extended" approach, the CICD workflow would be divided in more stages, probably with more project, but since the requirement just ask for a "CICD workflow", the current workflow is one considered easily understandable.

### HPA is configured and working on production deployment of `openshift-tasks`

The HPA is enabled using the playbook number 16 of the playbooks folder.

```
$ oc get hpa tasks 
NAME      REFERENCE                TARGETS           MINPODS   MAXPODS   REPLICAS   AGE
tasks     DeploymentConfig/tasks   <unknown> / 80%   1         10        1          1m
```

## Multitenancy

### Multiple Clients created

There are three "extra projects" created by default when this environment is deployed, `alpha`, `beta` and `common`.

There are also two groups, alpha_corp, which has the users `amy` and `andrew`, and beta_corp, with the users `brian` and `betty`.

TODO why the COÑO ahora los usuarios de alpha ven todo (si elimino el misterioso admin de clusterrolebindings, aunque sean admin de su proyecto, ya no lo ven.)

### Dedicated node for each Client

The node1 is reserved for the alpha client, while the node2 is reserved for the beta client.

As a learning practice, it has been implemented in two different ways, following the previous rules.

First of all, the projects alpha and beta have a nodeSelector with client=alpha and client=beta, so any pod deployed on them will go to their pods.

At the same time, the PodNodeSelector admission control plugin is enabled at the masters so the cluster default node selector is `client=common`, the selector for the alpha group, `client=alpha`, and the selector for the beta group `client=beta`

### `admissionControl` plugin sets limits per label (client/customer)

At the time when this assignment was written, the team (me) was not able to find a plugin called `admissionControl` to set any limit per label.

However, **ProjectRequestLimit** is probably the requested plugin, and it is enabled at the master-config file of each master with a simple set of limits using the "level" tag.

### The new project template is modified so that it includes a LimitRange

The Project Request Template is changed to the one created as default/project-request

### The new user template is used to create a user object with the specific label value

It is performed using oc new-user and adding a label. I could not find a "new user template".

### On-boarding new client documentation explains how to create a new client/customer
