#!/bin/bash
#Get the kubeconfig at the bastion
ansible masters[0] -b -m fetch -a "src=/root/.kube/config dest=/root/.kube/config flat=yes"
