# 2.1. Basic and HA Requirements

- Ability to authenticate at the master console

There are different users available to authenticate at the master console.

Amy, Andrew, Betty, Brian and Sergio.

- Registry has storage attached and working

$ oc get pv registry-volume
NAME              CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS    CLAIM                    STORAGECLASS   REASON    AGE
registry-volume   20Gi       RWX            Retain           Bound     default/registry-claim                            1h


- Router is configured on each infranode

$ oc get pods -n default -o wide | grep router
router-1-4blld             1/1       Running   0          1h        192.199.0.29    infranode2.bb62.internal
router-1-sh9m6             1/1       Running   0          1h        192.199.0.171   infranode1.bb62.internal


- PVs of different types are available for users to consume

Running oc get pv will show that there are 25 persistent volumes available with a size of 5GiB, and 25 persistent volumes with a size of 10GiB

- Ability to deploy a simple app (nodejs-mongo-persistent)

The app nodejs-mongo-persistent has been deployed at the demos project by running `oc new-app nodejs-mongo-persistent -n demos`.

Its route is available at the URL https://nodejs-mongo-persistent-demos.apps.bb62.example.opentlc.com

- There are three masters working

It is possible to check that there are three masters
ansible masters -m shell -a "ps -aux | grep ^etcd"


- There are three etcd instances working

ansible masters -m shell -a "ps -aux | grep ^etcd"


- There is a load balancer to access the masters called loadbalancer.$GUID.$DOMAIN

True

- There is a load balancer/DNS for both infranodes called *.apps.$GUID.$DOMAIN

True

- There are at least two infranodes, labeled env=infra

$ oc describe node infranode2.bb62.internal infranode1.bb62.internal | grep env=infra -B 3
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    cluster=bb62
                    env=infra
--
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    cluster=bb62
                    env=infra


# 2.2 Environment configuration

- NetworkPolicy is configured and working with projects isolated by default (simulate Multitenancy)

The inventory was modified so the installation sets up OpenShift using NetworkPolicy as the default SDN plugin.

Then, the new project template was modified so the new projects allow network connectons from the same project and the default, so the routers can access to the pods.

- Aggregated logging is configured and working

- Metrics collection is configured and working

- Router and Registry Pods run on Infranodes

- Metrics and Logging components run on Infranodes

- Service Catalog, Template Service Broker, and Ansible Service Broker are all working


# CICD Workflow

Jenkins is running with a persistent volume attached, and it has been used to deploy the tasks app with a extremely simple pipeline.

The route where the tasks app has its web server listening is tasks-jenkins.apps.bb62.example.opentlc.com

Then, the Horizontal Pod Autoscaler is enabled for the deployed app with 1-10 pods and a trigger with a usage of 80% of the CPU.


# Multitenancy

The users amy and andrew are in the alpha_corp group, which pods can be deployed on the client=alpha labeled node (node1), while brian and betty are in the beta_corp group, which pods can be deployed on the client=beta labeled node (node2).

There are also two admissionControl plugins, ProjectRequestLimitConfig and PodNodeSelector, so there are limits about requesting project for different users labeled with the "level" label, and also dedicated nodes for the clients.
