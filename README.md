# How to deploy the "Homework Openshift Environment"

This document explains how to deploy OpenShift to get the environment required at the "Assingment Lab" of the "OCP Advanced Deployment" course.

## Set the GUID

Some files require the GUID of the environment.

To set it, just run:

```bash
ansible-playbook build_files.yaml --extra-vars="GUID=INSERT_YOUR_GUID_HERE"
```

## Deploy 

Once the files have been generated, just run the following command to set the pre-requisites, deploy OpenShift, and perform the post-installation tasks.

```bash
ansible-playbook deploy.yaml -f 20
```
